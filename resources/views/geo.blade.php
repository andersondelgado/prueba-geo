<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Geo</title>
    <link rel="stylesheet" href="{!! asset('/js/leaflet/leaflet.css') !!}">
    <style>
        #map {
            height: 580px;
        }
    </style>
</head>
<body>
<input type="text" placeholder="buscar" id="search">
<button type="submit" id="send">enviar</button>
<div id="map"></div>
<div id="status"></div>

<script src="{!! asset('/js/leaflet/leaflet.js') !!}"></script>
<script src="{!! asset('/bower_components/jquery/dist/jquery.min.js') !!}"></script>
{{--<script src="{!! asset('/bower_components/jquery/dist/jquery.slim.min.js') !!}"></script>--}}
{{--<script src="{!! asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') !!}"></script>--}}
<script>
    jQuery(document).ready(function () {
        var watchId;

        if (navigator.geolocation) {
            browserSupportFlag = true;
            watchId = navigator.geolocation.watchPosition(successCallBack, errorCallBack, options);
        } else {
            alert("Tu navegador no soporta la geolocalización, actualiza tu navegador.");
        }

        var options = {
            enableHighAccuracy: true,
            timeout: 1000,
            maximumAge: 2000
        };

        function stop() {
            navigator.geolocation.clearWatch(watchId);
        }

        function errorCallBack(error) {
            var strMessage = "";

            // Check for known errors
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    strMessage = "Access to your location is turned off. " +
                            "Change your settings to turn it back on.";
                    break;
                case error.POSITION_UNAVAILABLE:
                    strMessage = "Data from location services is " +
                            "currently unavailable.";
                    break;
                case error.TIMEOUT:
                    strMessage = "Location could not be determined " +
                            "within a specified timeout period.";
                    break;
                default:
                    break;
            }

            document.getElementById("status").innerHTML = strMessage;
        }

        function coord() {
            var url = "<?php echo URL::to('/places') ?>";
            var datas = [];
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                cache: false,
                async: false,
                success: function (data) {
                    //console.log(JSON.stringify(data));
                    datas = data.data;
                }
            });

            return datas;
        }


        var search = $('#search').val();
        var url = "<?php echo URL::to('/places1/'); ?>";
        var datas2 = [];
        $('#send').click(function () {
            $.ajax({
                url: url,
                type: 'POST',
                data:{"search":search},
                dataType: 'json',
                cache: false,
                async: false,
                success: function (data) {
                    //console.log(JSON.stringify(data));
                    datas2.push(data.data);
                }
            });

            return datas2;
        });


        console.log(JSON.stringify(coord()));

        function latLong() {
            var arr = coord();
            var arr1 = [];
            for (var i = 0; i < arr.length; i++) {
                arr1.push(arr[i]["lat"], arr[i]["lng"]);
            }
            return arr1;
        }

        console.log("latlong: " + (latLong()));

        function successCallBack(pos) {
            var lat = pos.coords.latitude;
            var lng = pos.coords.longitude;
//        var accuracy = pos.coords.accuracy;
//        var date = new Date(pos.timestamp);
            //var map = L.map('map').setView([51.505, -0.09], 13);
            var map = L.map('map').setView([lat, lng], 13);

            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);

//            console.log(JSON.stringify(coord()));
            L.marker([lat, lng]).addTo(map)
                    .bindPopup('Mi ubicación')
                    .openPopup();

            var coords = coord();
            for (var i = 0; i < coords.length; i++) {
                var lat1 = coords[i]["lat"];
                var lng1 = coords[i]["lng"];
                var point = coords[i]["name"];
                L.marker([lat1, lng1]).addTo(map)
                        .bindPopup(point)
                        .openPopup();

                // L.polygon([lat1, lng1]).addTo(map);
            }

            var coords1 = datas2;
            if(coords1 !==undefined) {
                for (var i = 0; i < coords1.length; i++) {
                    var lat11 = coords[i]["lat"];
                    var lng11 = coords[i]["lng"];
                    var point11 = coords[i]["name"];
                    L.marker([lat11, lng11]).addTo(map)
                            .bindPopup(point11)
                            .openPopup();

                    // L.polygon([lat1, lng1]).addTo(map);
                }
            }

        }

    });
</script>
</body>
</html>