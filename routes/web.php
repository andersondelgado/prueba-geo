<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/place',function(){
//    $payload='[
//    {"name":"hospital clinicas caracas","address":"av. vollmer","lat":"10.5099967","lng":"-66.9011219"},
//    {"name":"farmatodo","address":"av. vollmer","lat":"10.5104674","lng":"-66.9029833"},
//    {"name":"farmatodo","address":"av. vollmer","lat":"10.5127515","lng":"-66.9032476"}
//    ]';
//
//    $json=json_decode($payload,true);
//    foreach($json as $value){
//        echo $value["name"];
//        $add=new App\address();
//        $add->name=$value["name"];
//        $add->address=$value["address"];
//        $add->lat=$value["lat"];
//        $add->lng=$value["lng"];
//
//        $add->save();
//    }
//
//    return response()->json(["message"=>"success"]);
//
//});

Route::post('/places1/', function (Request $request) {
    try {
        $name=$request->input('search');
        $add = App\address::where('name', 'like', '%' . $name . '%')->firstOrFail();
        return response()->json(["success" => true, "data" => $add]);
    } catch (Exception $e) {
        return response()->json(["success" => false, "data" => null]);
    }
});

Route::get('/places', function () {
    try {
        $model = App\address::all();
        return response()->json(["success" => true, "data" => $model->toArray()]);
    } catch (Exception $e) {
        return response()->json(["success" => false, "data" => null]);
    }
});

Route::get('/index', 'GeoController@index');